# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=gtavc_grub_theme
pkgname=("${pkgbase}-no_icons" "${pkgbase}-with_icons")
pkgver=1.0
pkgrel=10
pkgdesc="Inspired from Gta Vice City Game Menu."
arch=("any")
url="https://www.gnome-look.org/p/1233174"
license=('none')
install=grub-theme.install
source=("${pkgbase}-${pkgver}.tar.gz::https://github.com/kshitijsubedi/${pkgbase}/archive/${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}-with_icons.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}-no_icons.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgbase}/gtavc"-{48,64,128}.png
        'fix.patch')
sha256sums=('ae29719d0d48ad7c0712a41e7b495219d5f3284a968d390c288ac52b4fce652d'
            '658062d1729236aba4e063fbd737fd5b8e973e841670c58fe6421ce1ca36d1fd'
            '68e063aa309e3e390c3965cbfddb517ccf42ee42f2f02f03402bf41d881c7579'
            '9be90476553c7fb8d984d9a9189ca2e5ad185486aa168ca9757d575f714de488'
            'd8a1f352b005239d34916e7053998fbd04c358cecd98b219f4b1243690e8cea8'
            '951a886b394dd1de05c51af3faa3f91184e71c716088436ac5a6ed5ce952c449'
            'c202c9fd019c4e61753071e0ccde3cf03718df6ebe570c91448184578e2aaebb')

prepare() {
    cd ${srcdir}/${pkgbase}-${pkgver}/Gtavc_grub_with_icons/Gtavc_i
    patch -p1 -i ${srcdir}/fix.patch

    cd ${srcdir}/${pkgbase}-${pkgver}/Gtavc_grub_no_icons/Gtavc
    patch -p1 -i ${srcdir}/fix.patch
}

_gtavc_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_gtavc_desktop" | tee com.${pkgbase//-/_}.desktop
}

package_gtavc_grub_theme-no_icons() {
    depends=('grub')

    cd ${srcdir}/${pkgbase}-${pkgver}/Gtavc_grub_no_icons
    install -dm755 ${pkgdir}/boot/grub/themes/${pkgname}
    cp -r Gtavc/* ${pkgdir}/boot/grub/themes/${pkgname}

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgbase//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/gtavc-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:PACKAGE=.*:PACKAGE=${pkgname}:" "${startdir}/grub-theme.install"
}

package_gtavc_grub_theme-with_icons() {
    depends=('grub')

    cd ${srcdir}/${pkgbase}-${pkgver}/Gtavc_grub_with_icons
    install -dm755 ${pkgdir}/boot/grub/themes/${pkgname}
    cp -r Gtavc_i/* ${pkgdir}/boot/grub/themes/${pkgname}

    # Fix icon to dual boot with two versions of Manjaro
    cp -r ${pkgdir}/boot/grub/themes/${pkgname}/icons/manjaro.png ${pkgdir}/boot/grub/themes/${pkgname}/icons/manjarolinux.png

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgbase//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/gtavc-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:PACKAGE=.*:PACKAGE=${pkgname}:" "${startdir}/grub-theme.install"
}
